<?php

namespace Drupal\graph_springy\Plugin\GraphEngine;

use Drupal\graphapi\Plugin\GraphEngine\GraphEngineBase;

/**
 * Graph engine for Springy.
 *
 * @GraphEngine(
 *   id = "springy",
 *   label = @Translation("Springy"),
 * )
 */
class Springy extends GraphEngineBase {

  /**
   * {@inheritdoc}
   */
  public function preRender($element): array {
    $element['#attached']['library'][] = 'graph_springy/graph_springy';

    return $element;
  }

}
