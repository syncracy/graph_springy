<?php

namespace Drupal\graph_springy\Plugin\GraphFormat;

use Drupal\graphapi\Plugin\GraphFormat\GraphFormatBase;
use Drupal\Component\Utility\Html;
use Drupal\Component\Render\FormattableMarkup;

/**
 * Graph format for Springy.
 *
 * Supports the following attributes on graph vertices:
 *  - title: The label of the vertex.
 *
 * Supports the following attributes on graph edges:
 *  - title: The label of the edge.
 *
 * @GraphFormat(
 *   id = "springy",
 *   label = @Translation("Springy"),
 *   engine = "springy",
 *   supported_attributes = {
 *     "edge" = {
 *       "title"
 *     },
 *   },
 * )
 */
class Springy extends GraphFormatBase {

  /**
   * {@inheritdoc}
   */
  public function preProcess(&$variables) {
    $graph = $variables['graph'];

    $graph_array = [];

    // Convert the graph to the D7 array format so I don't have to rewrite the
    // JS script!
    foreach ($graph->getVertices() as $vertex) {
      $vertex_id = $vertex->getId();
      $graph_array[$vertex_id]['data']['title'] = $vertex->getAttribute('title') ?? $vertex_id;
    }

    foreach ($graph->getEdges() as $edge) {
      $start = $edge->getVertexStart()->getId();
      $end = $edge->getVertexEnd()->getId();

      $graph_array[$start]['edges'][$end] = [
        'title' => $edge->getAttribute('title') ?? '',
      ];
    }

    // Create a unique ID for this graph.
    $html_id = Html::getUniqueId('graph_springy_graphapi');

    $variables['#attached']['drupalSettings']['graph_springy']['graphs'][] = [
      'id' => $html_id,
      'graph' => $graph_array,
    ];

    // Create the container canvas.
    $variables['content'] = new FormattableMarkup('<canvas id="@id" width="@width" height="@height"/>', [
      '@id' => $html_id,
      '@width' => $variables['width'],
      '@height' => $variables['height'],
    ]);
  }

}
